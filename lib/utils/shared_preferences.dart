import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class AppSharedPreferences {
  SharedPreferences? _sharedPreferences;
  static const String USER_DATA = 'user_data';
  static const String ACCESS_TOKEN = 'access_token';

  Future<void> saveAccessToken(String accessToken) async {
    _sharedPreferences = await SharedPreferences.getInstance();
    await _sharedPreferences!.setString(ACCESS_TOKEN, accessToken);
  }

  Future<String?> getAccessToken() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    return _sharedPreferences!.getString(ACCESS_TOKEN);
  }
}
