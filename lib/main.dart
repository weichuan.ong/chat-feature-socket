import 'package:chat_socket/models/requests/request_login.dart';
import 'package:chat_socket/modules/conversation/chat_screen.dart';
import 'package:chat_socket/network/api_client.dart';
import 'package:chat_socket/network/api_exceptions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Chat',
      routes: {
        '/': (context) => const MyHomePage(title: 'Flutter Chat Socket Demo'),
        '/chat': (context) => ChatScreen()
      },
      initialRoute: '/',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController emailTextController = TextEditingController();
  final FocusNode emailFocusNode = FocusNode();
  final TextEditingController passwordTextController = TextEditingController();
  final FocusNode passwordFocusNode = FocusNode();
  final TextEditingController urlTextController = TextEditingController();
  final FocusNode urlFocusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const Text(
              'Login',
              style: TextStyle(fontSize: 28),
            ),
            TextField(
              textInputAction: TextInputAction.next,
              keyboardType: TextInputType.emailAddress,
              controller: emailTextController,
              focusNode: emailFocusNode,
              style: const TextStyle(color: Colors.black, fontSize: 18),
              cursorColor: Theme.of(context).primaryColor,
              decoration: InputDecoration(hintText: 'Email'),
            ),
            TextField(
              controller: passwordTextController,
              focusNode: passwordFocusNode,
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              style: const TextStyle(color: Colors.black, fontSize: 18),
              cursorColor: Theme.of(context).primaryColor,
              decoration: InputDecoration(hintText: 'Password'),
            ),
            TextField(
              controller: urlTextController,
              focusNode: urlFocusNode,
              keyboardType: TextInputType.url,
              style: const TextStyle(color: Colors.black, fontSize: 18),
              cursorColor: Theme.of(context).primaryColor,
              decoration: InputDecoration(hintText: 'HTTP'),
            ),
            TextButton(
                onPressed: () {
                  if (urlTextController.text.isNotEmpty) {
                    _loginUser();
                  }
                },
                child: Container(
                  margin: const EdgeInsets.only(top: 16),
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  alignment: Alignment.center,
                  child: const Text(
                    "Enter chat room",
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  color: Theme.of(context).primaryColor,
                ))
          ],
        ),
      ),
    );
  }

  void _loginUser() {
    final email = emailTextController.text;
    final password = passwordTextController.text;
    ApiClient.instance
        .login(RequestLogin(email: email, password: password))
        .then((value) {
      ApiClient.baseUrl = urlTextController.text;
      Navigator.pushNamed(context, '/chat', arguments: value.data);
    }).catchError((error) {
      final errorMessage = ApiExceptions.fromError(error).getErrorMessage();
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(errorMessage)));
    });
  }
}
