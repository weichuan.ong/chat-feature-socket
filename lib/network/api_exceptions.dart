import 'dart:collection';
import 'dart:convert';

import 'package:dio/dio.dart';

class ApiExceptions implements Exception {
  int? _statusCode;
  String _errorMessage = '';

  int? getStatusCode() {
    return _statusCode;
  }

  String getErrorMessage() {
    return _errorMessage;
  }

  ApiExceptions.fromError(dynamic error) {
    if (error is DioError) {
      _handleDioError(error);
    } else {
      _errorMessage = error.toString();
    }
  }

  _handleDioError(DioError dioError) {
    switch (dioError.type) {
      case DioErrorType.cancel:
        _errorMessage = 'Request was cancelled';
        break;
      case DioErrorType.connectTimeout:
        _errorMessage = 'Connection timeout';
        break;
      case DioErrorType.receiveTimeout:
        _errorMessage = 'Receive timeout in connection';
        break;
      case DioErrorType.sendTimeout:
        _errorMessage = 'Send timeout in connection with server';
        break;
      case DioErrorType.response:
        _statusCode = dioError.response!.statusCode;
        _errorMessage = _handleResponseError(_statusCode!, dioError.response!.data);
        break;
      default:
        _errorMessage = 'Something went wrong';
        break;
    }
  }

  String _handleResponseError(int statusCode, dynamic error) {
    switch (statusCode) {
      case 400:
        var errorData = error;
        if (error is String) {
          errorData = jsonDecode(error);
        }
        return _getBadRequestMessage(errorData['message']);
      case 401:
        return 'Unauthorized';
      case 403:
        return 'Forbidden';
      case 404:
        return error['message'];
      case 405:
        return 'Method not allowed';
      case 429:
        return 'Too many requests';
      case 431:
        return 'Request headers field too large';
      case 500:
        return 'Internal server error';
      default:
        return 'Oops something went wrong';
    }
  }

  String _getBadRequestMessage(dynamic error) {
    String message = '';
    LinkedHashMap<dynamic, dynamic> map = error.map((a, b) => MapEntry(a as String, b as List));
    map.forEach((key, value) {
      value.forEach((element) {
        message += '$element \n';
      });
    });

    return message;
  }
}