// ignore_for_file: non_constant_identifier_names

import 'dart:io';

import 'package:chat_socket/models/chat_message_model.dart';
import 'package:chat_socket/models/conversation_model.dart';
import 'package:chat_socket/models/requests/request_create_conversation.dart';
import 'package:chat_socket/models/requests/request_login.dart';
import 'package:chat_socket/models/responses/base_response.dart';
import 'package:chat_socket/models/user_model.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:retrofit/retrofit.dart';
part 'rest_client.g.dart';

@RestApi()
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  //Authentication
  @DELETE("auth/")
  Future<BaseResponse> logout();

  @POST("auth/")
  Future<BaseResponse<UserModel>> login(@Body() RequestLogin requestLogin);

  //Conversation
  @POST("conversations/")
  Future<BaseResponse<ConversationModel>> createConversation(
      @Body() RequsetCreateConversation requsetCreateConversation);

  @GET("conversations/history/{id}")
  Future<BaseResponse<List<ChatMessageModel>>> getConversationHistroy(
      @Path("id") String id);

  @POST("media/images/conversation/{id}")
  @MultiPart()
  @Headers({
    "Accept": "multipart/form-data",
  })
  Future<BaseResponse<ConversationModel>> postConversationImage(
      @Path("id") String id, @Part(name: "image") File file);
}
