import 'package:chat_socket/utils/shared_preferences.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'rest_client.dart';

class ApiClient {
  static final Dio _dio = _createDio();
  static final _sharedPref = AppSharedPreferences();
  static String baseUrl = "http://192.168.0.229:3000";

  static Dio _createDio() {
    // final headers = {
    //   'Accept': 'application/json',
    //   'Access-Control-Allow-Origin': '*',
    //   'Access-Control-Allow-Credentials': true,
    //   'Access-Control-Allow-Methods': 'POST,GET,PUT,DELETE,OPTIONS',
    // };
    return Dio(
      BaseOptions(
        contentType: Headers.jsonContentType,
        connectTimeout: 15 * 1000,
        receiveTimeout: 15 * 1000,
      ),
    );
  }

  static RestClient _restClient() {
    if (!kReleaseMode) {
      _dio.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        compact: false,
      ));
    }
    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions requestOptions,
            RequestInterceptorHandler requestInterceptorHandler) async {
          requestOptions.contentType = Headers.jsonContentType;
          await _sharedPref.getAccessToken().then((token) {
            if (token != null) {
              requestOptions.headers['x-auth-token'] = token;
              print(requestOptions.headers['x-auth-token']);
            }
          });
          return requestInterceptorHandler.next(requestOptions);
        },
        onResponse: (Response<dynamic> response,
            ResponseInterceptorHandler responseInterceptorHandler) {
          if (response.headers['x-auth-token'] != null) {
            updateAccessToken(response.headers['x-auth-token']!.first);
          }
          return responseInterceptorHandler.next(response);
        },
        onError: (DioError dioError,
            ErrorInterceptorHandler errorInterceptorHandler) {
          // try {
          //   if (dioError.response?.statusCode == 401) {
          //     var errorResponse = dioError.response?.data;
          //   }
          // } catch (e) {
          //   print(ApiExceptions.fromError(e).getErrorMessage());
          // }

          return errorInterceptorHandler.next(dioError);
        },
      ),
    );
    return RestClient(_dio, baseUrl: baseUrl + "/api/");
  }

  static updateAccessToken(String? accessToken) async {
    if (accessToken != null) await _sharedPref.saveAccessToken(accessToken);
    String? token = await _sharedPref.getAccessToken();
    _dio.options.headers["x-auth-token"] = token;
  }

  static final RestClient _instance = _restClient();

  static RestClient get instance {
    return _instance;
  }
}
