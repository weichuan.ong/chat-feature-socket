import 'dart:io';

import 'package:chat_socket/models/attachment_data_model.dart';
import 'package:chat_socket/models/attachment_model.dart';
import 'package:chat_socket/models/chat_message_model.dart';
import 'package:chat_socket/models/send_message_model.dart';
import 'package:chat_socket/models/user_model.dart';
import 'package:chat_socket/network/api_client.dart';
import 'package:chat_socket/utils/shared_preferences.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:uuid/uuid.dart';
import 'package:http_parser/http_parser.dart';
import 'package:intl/intl.dart';

class ChatScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final TextEditingController _textEditingController = TextEditingController();
  late Socket socket;
  List<ChatMessageModel> conversations = [];
  late UserModel _userModel;
  String typerName = '';
  final ScrollController _controller = ScrollController();

  @override
  void initState() {
    try {
      socket = io(ApiClient.baseUrl, <String, dynamic>{
        'transports': ['websocket', 'polling'],
        'autoConnect': false
      });
      socket.connect();

      socket.onDisconnect((data) {
        print('disconnected');
        print(data);
      });

      socket.onConnecting((data) {
        print('trying to connect');
        print(data);
      });

      socket.onConnect((_) {
        print('connected');
        socket.on("receivedMessage", (data) {
          final message = ChatMessageModel.fromJson(data);
          conversations.add(message);
          setState(() {});
          _controller.animateTo(_controller.position.maxScrollExtent,
              curve: Curves.easeOut, duration: const Duration(seconds: 1));
          socket.emit('acknowledge', {
            "conversation": "618bfff1effc9c157c50f76a",
            "messageId": message.id,
            "from": _userModel.id
          });
        });
        socket.on("ackMessage", (data) {
          var messageId = data['_id'];
          var status = data['status'];
          for (var message in conversations) {
            if (message.id == messageId) {
              setState(() {
                message.status = status;
              });
            }
          }
        });
        socket.on("readMessage", (data) {});
        socket.on("receivedTyping", (data) {
          typerName = data['postedByName'];
          Future.delayed(const Duration(seconds: 3), () {
            setState(() {
              typerName = '';
            });
          });
        });
        socket.emit("conversation", "618bfff1effc9c157c50f76a");
      });
    } catch (e) {
      print(e.toString());
    }

    super.initState();
  }

  @override
  void dispose() {
    socket.off("receivedMessage");
    socket.off("ackMessage");
    socket.off("readMessage");
    socket.off("typing");
    socket.disconnect();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _userModel = ModalRoute.of(context)!.settings.arguments as UserModel;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          (typerName.isNotEmpty) ? '$typerName is typing...' : "",
          style: const TextStyle(
              color: Colors.white, fontSize: 20, fontStyle: FontStyle.italic),
        ),
      ),
      body: Stack(
        children: [
          _getChatBox(),
          Container(
            padding: const EdgeInsets.fromLTRB(8, 16, 8, 16),
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                _getChatBar(),
              ],
            ),
          )
        ],
      ),
      resizeToAvoidBottomInset: true,
    );
  }

  Widget _getChatBox() => (conversations.isNotEmpty)
      ? Container(
          padding: EdgeInsets.only(bottom: 80),
          height: MediaQuery.of(context).size.height,
          child: ListView.builder(
              controller: _controller,
              itemCount: conversations.length,
              itemBuilder: (context, i) {
                final message = conversations[i];
                return (message.postedBy == _userModel.id)
                    ? _senderChatBubble(message)
                    : _receiverChatBubble(message);
              }),
        )
      : const SizedBox.shrink();

  Widget _senderChatBubble(ChatMessageModel message) => Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.green),
                child: Column(children: [
                  (message.attachments!.isEmpty)
                      ? Text(
                          message.message!,
                          style: const TextStyle(
                              color: Colors.white, fontSize: 18),
                        )
                      : FutureBuilder<String?>(
                          future: AppSharedPreferences().getAccessToken(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.done) {
                              if (snapshot.hasData) {
                                final token = snapshot.data!;
                                return SizedBox(
                                  height: MediaQuery.of(context).size.width / 2,
                                  child: Image.network(
                                    "${ApiClient.baseUrl}media/images/${message.attachments!.first.refId}",
                                    headers: {"x-auth-token": token},
                                  ),
                                );
                              }
                            }
                            return const CircularProgressIndicator();
                          }),
                ])),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  convertDateTime(message.dateCreation!),
                  style: const TextStyle(fontSize: 10),
                ),
                const SizedBox(
                  width: 2,
                ),
                (message.status == 'S')
                    ? const Icon(
                        Icons.check,
                        size: 12,
                      )
                    : Row(
                        children: const [
                          Icon(
                            Icons.check,
                            size: 12,
                          ),
                          Icon(
                            Icons.check,
                            size: 12,
                          )
                        ],
                      )
              ],
            ),
          ],
        ),
      );
  Widget _receiverChatBubble(ChatMessageModel message) => Container(
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.amberAccent),
                child: Column(children: [
                  (message.attachments!.isEmpty)
                      ? Text(
                          message.message!,
                          style: const TextStyle(
                              color: Colors.black, fontSize: 18),
                        )
                      : FutureBuilder<String?>(
                          future: AppSharedPreferences().getAccessToken(),
                          builder: (context, snapshot) {
                            if (snapshot.connectionState ==
                                ConnectionState.done) {
                              if (snapshot.hasData) {
                                final token = snapshot.data!;
                                return SizedBox(
                                  height: MediaQuery.of(context).size.width / 2,
                                  child: Image.network(
                                    "${ApiClient.baseUrl}media/images/${message.attachments!.first.refId}",
                                    headers: {"x-auth-token": token},
                                  ),
                                );
                              }
                            }
                            return const CircularProgressIndicator(
                              color: Colors.white,
                            );
                          }),
                ])),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  convertDateTime(message.dateCreation!),
                  style: const TextStyle(fontSize: 12),
                ),
                // const SizedBox(
                //   width: 16,
                // ),
                Text(
                  message.postedByName!,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor, fontSize: 12),
                ),
              ],
            ),
          ],
        ),
      );

  Widget _getChatBar() => Container(
        decoration: BoxDecoration(border: Border.all(), color: Colors.white),
        padding: EdgeInsets.all(8),
        height: 48,
        child: Row(
          children: [
            Expanded(
              child: TextField(
                textInputAction: TextInputAction.send,
                keyboardType: TextInputType.text,
                controller: _textEditingController,
                onChanged: (text) {
                  if (text.isNotEmpty) {
                    emitTyping();
                  }
                },
                onSubmitted: (text) {
                  if (text.isNotEmpty) {
                    attemptSendMessage([]);
                  }
                },
              ),
            ),
            IconButton(
                onPressed: () {
                  postConversationImageFromGallery();
                },
                icon: const Icon(
                  Icons.image,
                )),
            IconButton(
                onPressed: () {
                  postConversationFromCamera();
                },
                icon: const Icon(
                  Icons.camera,
                )),
          ],
        ),
      );

  void emitTyping() {
    socket.emit('typing', {
      "conversation": "618bfff1effc9c157c50f76a",
      "from": _userModel.id,
      "firstname": _userModel.firstname,
      "profileImage": _userModel.profileImage
    });
  }

  void attemptSendMessage(List<AttachmentModel> attachments) {
    final message = _textEditingController.text.toString();
    String foreignChatId = const Uuid().v1();
    socket.emit(
        'sendMessage',
        SendMessageModel(
                conversation: "618bfff1effc9c157c50f76a",
                foreignChatId: foreignChatId,
                message: message,
                from: _userModel.id,
                profileImage: _userModel.profileImage,
                firstname: _userModel.firstname,
                attachments: attachments)
            .toJson());
    _textEditingController.clear();
  }

  void postConversationImageFromGallery() async {
    final image = await ImagePicker()
        .pickImage(source: ImageSource.gallery, imageQuality: 50);
    if (image != null) {
      final attachment = await mediaUpload(File(image.path), 'images');
      if (attachment != null) {
        attemptSendMessage(
            [AttachmentModel(refId: attachment.id, type: "image")]);
      }
    }
  }

  void postConversationFromCamera() async {
    final image = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 50);
    if (image != null) {
      final attachment = await mediaUpload(File(image.path), 'images');
      if (attachment != null) {
        attemptSendMessage(
            [AttachmentModel(refId: attachment.id, type: "image")]);
      }
    }
  }

  Future<AttachmentDataModel?> mediaUpload(File file, String type) async {
    String url = "";
    AttachmentDataModel? attachmentDataModel;
    MediaType? mediaType;
    switch (type) {
      case "images":
        url = "media/images/chat";
        mediaType = MediaType('image', 'jpeg');
        break;
      case "audio":
        url = "media/audios/chat";
        break;
      case "video":
        url = "media/videos/chat";
        mediaType = MediaType('video', 'mp4');
        break;
      default:
        url = "";
    }
    var baseOptions = BaseOptions(
      baseUrl: ApiClient.baseUrl,
      connectTimeout: 600 * 1000,
      receiveTimeout: 600 * 1000,
      headers: {
        'Accept': 'multipart/form-data',
        'x-auth-token': await AppSharedPreferences().getAccessToken(),
      },
    );
    final formData = FormData.fromMap({
      type: await MultipartFile.fromFile(file.path, contentType: mediaType)
    });

    Dio dio = Dio(baseOptions);
    await dio.post(url, data: formData).then((value) {
      if (value.statusCode == 200) {
        final response = value.data['data'].first;
        attachmentDataModel = AttachmentDataModel.fromJson(response);
      }
    }).catchError((error) {
      final errorMessage = error.toString();
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(errorMessage)));
    });
    return attachmentDataModel;
  }

  Future<String> getAccessToken() async {
    String token = '';
    await AppSharedPreferences()
        .getAccessToken()
        .then((value) => token = value ?? '');
    return token;
  }

  String convertDateTime(String dateTime) {
    DateTime dt = DateTime.parse(dateTime).toLocal();
    return DateFormat('hh:mm aa', 'en_US').format(dt);
  }
}
