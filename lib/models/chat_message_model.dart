import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

import 'package:chat_socket/models/attachment_model.dart';
part 'chat_message_model.g.dart';
@JsonSerializable()
class ChatMessageModel {
  @JsonKey(name:'_id')
  String? id;
  @JsonKey(name:'message_id')
  String? foreignChatId;
  String? dateCreation;
  String? dateUpdate;
  @JsonKey(name:'conversation_id')
  String? conversation;
  String? message;
  String? from;
  String? postedBy;
  String? postedByName;
  String? postedByProfile;
  String? status;
  List<AttachmentModel>? attachments;
  ChatMessageModel({
    this.id,
    this.foreignChatId,
    this.dateCreation,
    this.dateUpdate,
    this.conversation,
    this.message,
    this.from,
    this.postedBy,
    this.postedByName,
    this.postedByProfile,
    this.status,
    this.attachments,
  });



  Map<String,dynamic> toJson() => _$ChatMessageModelToJson(this);

  factory ChatMessageModel.fromJson(Map<String,dynamic> json) => _$ChatMessageModelFromJson(json);
}
