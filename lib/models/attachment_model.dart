
import 'package:json_annotation/json_annotation.dart';
part 'attachment_model.g.dart';

@JsonSerializable()
class AttachmentModel {
  String? type;
  String? refId;

  AttachmentModel({
    this.type,
    this.refId,
  });

  Map<String, dynamic> toJson() => _$AttachmentModelToJson(this);

  factory AttachmentModel.fromJson(Map<String, dynamic> json) =>
      _$AttachmentModelFromJson(json);
}
