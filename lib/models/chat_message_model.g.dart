// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_message_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatMessageModel _$ChatMessageModelFromJson(Map<String, dynamic> json) =>
    ChatMessageModel(
      id: json['_id'] as String?,
      foreignChatId: json['message_id'] as String?,
      dateCreation: json['dateCreation'] as String?,
      dateUpdate: json['dateUpdate'] as String?,
      conversation: json['conversation_id'] as String?,
      message: json['message'] as String?,
      from: json['from'] as String?,
      postedBy: json['postedBy'] as String?,
      postedByName: json['postedByName'] as String?,
      postedByProfile: json['postedByProfile'] as String?,
      status: json['status'] as String?,
      attachments: (json['attachments'] as List<dynamic>?)
          ?.map((e) => AttachmentModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ChatMessageModelToJson(ChatMessageModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'message_id': instance.foreignChatId,
      'dateCreation': instance.dateCreation,
      'dateUpdate': instance.dateUpdate,
      'conversation_id': instance.conversation,
      'message': instance.message,
      'from': instance.from,
      'postedBy': instance.postedBy,
      'postedByName': instance.postedByName,
      'postedByProfile': instance.postedByProfile,
      'status': instance.status,
      'attachments': instance.attachments,
    };
