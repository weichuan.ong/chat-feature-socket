import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'request_login.g.dart';
@JsonSerializable()
class RequestLogin {
  String? email;
  String? password;
  RequestLogin({
    this.email,
    this.password,
  });

  Map<String, dynamic> toJson() => _$RequestLoginToJson(this);

  factory RequestLogin.fromJson(Map<String, dynamic> json) =>_$RequestLoginFromJson(json);
}
