import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'request_create_conversation.g.dart';
@JsonSerializable()
class RequsetCreateConversation {
  List<String>? pods;
  List<String>? members;
  List<String>? deviceMembers;
  RequsetCreateConversation({
    this.pods,
    this.members,
    this.deviceMembers,
  });

  Map<String, dynamic> toJson() =>_$RequsetCreateConversationToJson(this);

  factory RequsetCreateConversation.fromJson(Map<String, dynamic> json) =>_$RequsetCreateConversationFromJson(json);
}
