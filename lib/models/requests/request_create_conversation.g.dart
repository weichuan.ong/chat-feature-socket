// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_create_conversation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequsetCreateConversation _$RequsetCreateConversationFromJson(
        Map<String, dynamic> json) =>
    RequsetCreateConversation(
      pods: (json['pods'] as List<dynamic>?)?.map((e) => e as String).toList(),
      members:
          (json['members'] as List<dynamic>?)?.map((e) => e as String).toList(),
      deviceMembers: (json['deviceMembers'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
    );

Map<String, dynamic> _$RequsetCreateConversationToJson(
        RequsetCreateConversation instance) =>
    <String, dynamic>{
      'pods': instance.pods,
      'members': instance.members,
      'deviceMembers': instance.deviceMembers,
    };
