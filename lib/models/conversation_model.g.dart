// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'conversation_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConversationModel _$ConversationModelFromJson(Map<String, dynamic> json) =>
    ConversationModel(
      name: json['name'] as String?,
      members:
          (json['members'] as List<dynamic>?)?.map((e) => e as String).toList(),
      deviceMembers: (json['deviceMembers'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      pods: (json['pods'] as List<dynamic>?)?.map((e) => e as String).toList(),
      status: json['status'] as String?,
      id: json['_id'] as String?,
      sysName: json['sysName'] as String?,
      owner: json['owner'] as String?,
      userCreation: json['userCreation'] as String?,
      userUpdate: json['userUpdate'] as String?,
      dateCreation: json['dateCreation'] as String?,
      dateUpdate: json['dateUpdate'] as String?,
      image: json['image'] as String?,
    );

Map<String, dynamic> _$ConversationModelToJson(ConversationModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'members': instance.members,
      'deviceMembers': instance.deviceMembers,
      'pods': instance.pods,
      'status': instance.status,
      '_id': instance.id,
      'sysName': instance.sysName,
      'owner': instance.owner,
      'userCreation': instance.userCreation,
      'userUpdate': instance.userUpdate,
      'dateCreation': instance.dateCreation,
      'dateUpdate': instance.dateUpdate,
      'image': instance.image,
    };
