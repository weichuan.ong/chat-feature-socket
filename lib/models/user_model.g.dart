// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserModel _$UserModelFromJson(Map<String, dynamic> json) => UserModel(
      pods: (json['pods'] as List<dynamic>?)?.map((e) => e as String).toList(),
      conversations: (json['conversations'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      contacts: (json['contacts'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      deviceContacts: (json['deviceContacts'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      type: json['type'] as String?,
      status: json['status'] as String?,
      id: json['_id'] as String?,
      firstname: json['firstname'] as String?,
      lastname: json['lastname'] as String?,
      email: json['email'] as String?,
      dateCreation: json['dateCreation'] as String?,
      dateUpdate: json['dateUpdate'] as String?,
      lastLogin: json['lastLogin'] as String?,
      profileImage: json['profileImage'] as String?,
    );

Map<String, dynamic> _$UserModelToJson(UserModel instance) => <String, dynamic>{
      'pods': instance.pods,
      'conversations': instance.conversations,
      'contacts': instance.contacts,
      'deviceContacts': instance.deviceContacts,
      'type': instance.type,
      'status': instance.status,
      '_id': instance.id,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'email': instance.email,
      'dateCreation': instance.dateCreation,
      'dateUpdate': instance.dateUpdate,
      'lastLogin': instance.lastLogin,
      'profileImage': instance.profileImage,
    };
