// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'send_message_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SendMessageModel _$SendMessageModelFromJson(Map<String, dynamic> json) =>
    SendMessageModel(
      conversation: json['conversation'] as String?,
      foreignChatId: json['foreignChatId'] as String?,
      message: json['message'] as String?,
      from: json['from'] as String?,
      firstname: json['firstname'] as String?,
      profileImage: json['profileImage'] as String?,
      attachments: (json['attachments'] as List<dynamic>?)
          ?.map((e) => AttachmentModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SendMessageModelToJson(SendMessageModel instance) =>
    <String, dynamic>{
      'conversation': instance.conversation,
      'foreignChatId': instance.foreignChatId,
      'message': instance.message,
      'from': instance.from,
      'firstname': instance.firstname,
      'profileImage': instance.profileImage,
      'attachments': instance.attachments,
    };
