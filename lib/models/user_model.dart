import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
part 'user_model.g.dart';
@JsonSerializable()
class UserModel {
  List<String>? pods;
  List<String>? conversations;
  List<String>? contacts;
  List<String>? deviceContacts;
  String? type;
  String? status;
  @JsonKey(name: '_id')
  String? id;
  String? firstname;
  String? lastname;
  String? email;
  String? dateCreation;
  String? dateUpdate;
  String? lastLogin;
  String? profileImage;

  UserModel({
    this.pods,
    this.conversations,
    this.contacts,
    this.deviceContacts,
    this.type,
    this.status,
    this.id,
    this.firstname,
    this.lastname,
    this.email,
    this.dateCreation,
    this.dateUpdate,
    this.lastLogin,
    this.profileImage
  });

  Map<String,dynamic> toJson() => _$UserModelToJson(this);

  factory UserModel.fromJson(Map<String,dynamic> json) => _$UserModelFromJson(json);
}
