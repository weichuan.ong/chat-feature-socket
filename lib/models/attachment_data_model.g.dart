// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attachment_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AttachmentDataModel _$AttachmentDataModelFromJson(Map<String, dynamic> json) =>
    AttachmentDataModel(
      id: json['_id'] as String?,
      name: json['name'] as String?,
      mimeType: json['mimeType'] as String?,
      userUploaded: json['userUploaded'] as String?,
      dateUploaded: json['dateUploaded'] as String?,
      dateExpire: json['dateExpire'] as String?,
    );

Map<String, dynamic> _$AttachmentDataModelToJson(
        AttachmentDataModel instance) =>
    <String, dynamic>{
      '_id': instance.id,
      'name': instance.name,
      'mimeType': instance.mimeType,
      'userUploaded': instance.userUploaded,
      'dateUploaded': instance.dateUploaded,
      'dateExpire': instance.dateExpire,
    };
