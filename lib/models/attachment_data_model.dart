import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'attachment_data_model.g.dart';

@JsonSerializable()
class AttachmentDataModel {
  @JsonKey(name: '_id')
  String? id;
  String? name;
  String? mimeType;
  String? userUploaded;
  String? dateUploaded;
  String? dateExpire;
  AttachmentDataModel({
    this.id,
    this.name,
    this.mimeType,
    this.userUploaded,
    this.dateUploaded,
    this.dateExpire,
  });

  Map<String, dynamic> toJson() => _$AttachmentDataModelToJson(this);

  factory AttachmentDataModel.fromJson(Map<String, dynamic> json) =>
      _$AttachmentDataModelFromJson(json);
}
