import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

import 'package:chat_socket/models/attachment_model.dart';
part 'send_message_model.g.dart';
@JsonSerializable()
class SendMessageModel {
  String? conversation;
  String? foreignChatId;
  String? message;
  String? from;
  String? firstname;
  String? profileImage;
  List<AttachmentModel>? attachments;
  SendMessageModel({
    this.conversation,
    this.foreignChatId,
    this.message,
    this.from,
    this.firstname,
    this.profileImage,
    this.attachments,
  });

  Map<String,dynamic> toJson() => _$SendMessageModelToJson(this);

  factory SendMessageModel.fromJson(Map<String,dynamic> json) => _$SendMessageModelFromJson(json);
}
