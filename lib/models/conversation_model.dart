import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'conversation_model.g.dart';
@JsonSerializable()
class ConversationModel {
  String? name;
  List<String>? members;
  List<String>? deviceMembers;
  List<String>? pods;
  String? status;
  @JsonKey(name: '_id')
  String? id;
  String? sysName;
  String? owner;
  String? userCreation;
  String? userUpdate;
  String? dateCreation;
  String? dateUpdate;
  String? image;
  ConversationModel({
    this.name,
    this.members,
    this.deviceMembers,
    this.pods,
    this.status,
    this.id,
    this.sysName,
    this.owner,
    this.userCreation,
    this.userUpdate,
    this.dateCreation,
    this.dateUpdate,
    this.image,
  });

  Map<String,dynamic> toJson() =>_$ConversationModelToJson(this);

  factory ConversationModel.fromJson(Map<String,dynamic> json) => _$ConversationModelFromJson(json);
}
